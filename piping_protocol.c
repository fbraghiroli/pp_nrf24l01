/*
  Copyright (C) 2019 Federico Braghiroli
  Author: Federico Braghiroli <federico.braghiroli@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "piping_protocol.h"

#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#define dprintf(...) fprintf(stderr, __VA_ARGS__)

#define MAGIC0 0x10
#define MAGIC1 0xbe
#define MAGIC2 0xef
#define MAGIC3 0x10
#define SIZE_N 2 /* size: 2 bytes, big endian;  64k should be enough */

const unsigned char MAGIC[] = {0x10, 0xbe, 0xef, 0x10};

static inline void dump_mem(const unsigned char *b, size_t s);
static int decode_ppdata(struct pp_wdata *d);
static inline size_t frame_head(unsigned char *fr_p, size_t d_len);

int pp_init(struct pp_wdata *d)
{
	if (!d)
		return -EINVAL;
	d->fs = PP_FS_IDLE;
	d->magic_n = 0;

	return 0;
}

static inline void dump_mem(const unsigned char *b, size_t s)
{
	while (s--)
		dprintf("%02x ", *b++);
	dprintf("\n");
}

/* Decode and execute the pp frame received. If requested, it send back
 * a response containing the outcome of the operation.
 *
 * WARNING:
 * To save space we use the same frame_buf to build the response
 * frame.
 */
static int decode_ppdata(struct pp_wdata *d)
{
	size_t i = 0;
	int ret = 0;
	struct pp_header head;

	/* TODO: make a decent check of the frame length / payload */
	if (d->f_size < 7) {
		ret = EINVAL;
		goto exit_ret;
	}

	while (i < d->f_size)
		dprintf("%02X", d->frame_buf[i++]);
	dprintf("\n");

	head.rcr = PP_RCR(d->frame_buf[PP_PIPE_ID_OFFSET]);
	head.ptype = PP_PIPE_CTL(d->frame_buf[PP_PIPE_ID_OFFSET]) ?
		PP_PTYPE_CTL : PP_PTYPE_RADIO;
	head.psrc = PP_PIPE_SRC(d->frame_buf[PP_PIPE_ID_OFFSET]);
	memcpy(&head.addr, d->frame_buf+PP_ADDR_OFFSET, PP_PIPE_ADDR_LEN);

	if (head.ptype == PP_PTYPE_RADIO) {
		if (!d->recv_d) {
			ret = 0;
			goto exit_ret;
		}

		ret = d->recv_d(d, &head, &d->frame_buf[PP_WDATA_OFFSET],
				d->f_size - PP_WDATA_OFFSET);
	} else {
		if (!d->recv_ctl) {
			ret = 0;
			goto exit_ret;
		}

		ret = d->recv_ctl(d, &head, &d->frame_buf[PP_WDATA_OFFSET],
				  d->f_size - PP_WDATA_OFFSET);
	}

exit_ret:
	return -ret;
}

static inline size_t frame_head(unsigned char *fr_p, size_t d_len)
{
	*(fr_p++) = MAGIC[0];
	*(fr_p++) = MAGIC[1];
	*(fr_p++) = MAGIC[2];
	*(fr_p++) = MAGIC[3];
	*(fr_p++) = (d_len >> 8);
	*(fr_p++) = d_len & 0xff;
	return 6;
}

/* Encode return code response from either an ioctl or a write.
 *
 * WARNING: it does not check buffer size! It must be large enough to hold
 * an rcr frame! */
size_t pp_encode_rcr(unsigned char *fr_p, uint8_t pt_radio, uint32_t rc)
{
	unsigned char *pp_p = fr_p+FRAME_PP_OFFSET;

	memset(pp_p, 0, PP_WDATA_OFFSET); /* clear the pp header */
	*pp_p |= PP_RCR_MASK;
	*pp_p |= pt_radio ? 0 : (PP_PIPE_TYPE_MASK);
	pp_p += PP_WDATA_OFFSET;
	*(pp_p++) = (rc >> 24);
	*(pp_p++) = (rc >> 16);
	*(pp_p++) = (rc >> 8);
	*(pp_p++) = rc;
	frame_head(fr_p, pp_p-(fr_p+FRAME_PP_OFFSET));
	return pp_p-fr_p;
}

int pp_fetch(struct pp_wdata *d, const unsigned char *buf_in,
	     size_t len_in)
{
	int f_complete = 0;

	if (!d || !buf_in)
		return -EINVAL;

	while (len_in > 0) {
		switch(d->fs) {
		case PP_FS_IDLE:
			if (*buf_in == MAGIC[0]) {
				d->fs = PP_FS_MAGIC;
				d->magic_n = 1;
			}
			break;

		case PP_FS_MAGIC:
			if (d->magic_n == 4) {
				d->f_size = (*buf_in) << 8;
				d->fs = PP_FS_SIZE;
				break;
			}

			if (*buf_in == MAGIC[d->magic_n++]) {
				if (d->magic_n == 4)
					dprintf("Magic recv\n");
				break;
			} else {
				dprintf("Expected 0x%x, got 0x%x\n",
					MAGIC[d->magic_n-1], *buf_in);
				d->fs = PP_FS_IDLE;
			}
			break;
		case PP_FS_SIZE:
			d->f_size |= (*buf_in);
			d->f_left = d->f_size;
			if (d->f_size > FRAME_SIZE_MAX) {
				d->fs = PP_FS_IDLE; /* frame discarded */
			} else {
				dprintf("Size: %u\n", d->f_size);
				d->fs = PP_FS_DATA;
			}
			break;
		case PP_FS_DATA:
			d->frame_buf[d->f_size-d->f_left] = *buf_in;
			if (--d->f_left == 0) {
				f_complete++;
				d->fs = PP_FS_IDLE;
				decode_ppdata(d);
			}
			break;
		}
		buf_in++;
		len_in--;
	}

	return f_complete;
}

/* Read data from device and encode a frame to send.
 *
 * WARNING:
 * To save space, it is used the receiving buffer to build the response
 * frame
 */
#if 0
int pp_encode(unsigned char *buf, size_t buf_len, int dev_fd)
{
	size_t n_in = 0;
	unsigned char *data, *head;

	if (!buf)
		return -EINVAL;

	if (buf_len < FRAME_PP_OFFSET+PP_WDATA_OFFSET) {
		dprintf("Bigger buffer required\n");
		return -EINVAL;
	}
	data = buf+FRAME_PP_OFFSET+PP_WDATA_OFFSET;
	head = buf;

	if ((n_in = read(dev_fd, data, buf_len-(FRAME_PP_OFFSET+PP_WDATA_OFFSET))) == 0) {
		dprintf("nothing to read !?!?!\n");
		return -EIO;
	} else {
		uint32_t pipeno;
		frame_head(head, n_in + PP_WDATA_OFFSET);
		if (ioctl(dev_fd, NRF24L01IOC_GETLASTPIPENO,
			  (unsigned long)((uint32_t *)&pipeno)) < 0)
			return -errno;
		head += FRAME_PP_OFFSET;
		*(head++) = pipeno & PP_PIPE_SRC_MASK;
		memset(head, 0, PP_WDATA_OFFSET-PP_ADDR_OFFSET);
	}

	return n_in + FRAME_PP_OFFSET+PP_WDATA_OFFSET;
}
#endif
